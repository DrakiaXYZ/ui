import React from 'react'

export default () => {
  return (
    <div className='container'>
      <h1>We're Shutting Down</h1>
      <p>The Plum Tree First launched August 2016 which means it's been running fo over 7 years now.</p>
      <p>In that time its popularity has grown far beyond expectations and it's had a genuinely heart warming amount of love from our users.</p>
      <p>However it's with a heap of sadness I'm announcing the upcoming shutdown of Plum Tree for the reasons explained below.</p>
      <h2>Sunset</h2>
      <p>The Plum Tree will not shut down immediately. Instead we'll be going through a period of "sunset" to give users some time to make backups of their trees.</p>
      <p>Downloading trees is a recent feature addition. Go to any tree you want to keep, hit download and a <code>.zip</code> file containing your tree's data, images and a html file to view the tree will be downloaded.</p>
      <p>There's no official shutdown date yet. I want to give a reasonable amount of time to users to become aware and backup any trees they want. For this reason I'll keep an eye on how many people are making backups and use that information accordingly.</p>
      <h2>Why Shut Down</h2>
      <p>There's a few reasons but the primary one is costs. While I do get a regular set of donations, which I can't thank those contributors enough for, each month these donations generally fall short ~$100-$200 of the actual server running costs.</p>
      <p>Without the help of those people chipping in the Plum Tree would have likely shut down years ago.</p>
      <p>Another reason is time. Over the years my other life commitments have left little time for me to work on the Plum Tree. I've kept the site running, made optimizations to reduce costs and tried to keep things up to date. However that time given is far less than I had starting out with what was a fun side project. As it stands I don't have the time to add new features or really give the Plum Tree and its community the focus it deserves.</p>
      <p>The last reason to mention really just emphasises the previous two. As of October 1st me and my wife will be expanding our family with the birth of our first baby girl.</p>
      <p>While I know for many the Plum Tree will be missed I hope you can understand with even less time and financial spends to be prioritized elsewhere, it's time for me to take down the Plum Tree.</p>
      <h2>Thank You</h2>
      <p>It's been a fantastic ride. From those who have donated through the years, helped find bugs and suggest features, send in kind words or just used the Plum Tree I can't thank you all enough.</p>
      <p>The project has been fun and challenging in some ways but always a labour of love. I've loved the technical aspects of coding the project and had great pleasure from seeing people love something I created.</p>
      <p>A final thank you all - Tobias</p>
    </div>
  )
}
